# coding: utf-8
import random
import string

def get_from_file(file_o):
    l = []
    for line in file_o:
        l.append(line.strip('\n'))
    return l

def random_without_repeat(src_list, used_list):
    e = random.choice(src_list)
    if e in used_list:
        # print('Repeated med:', u_name)
        e = random_without_repeat(src_list, used_list)
    used_list.append(e)
    return e

def random_phone():
    s = '9'
    s += random.choice(['1', '2', '3', '6'])
    for _ in range(7):
        s += str(random.randint(0, 9))
    return s

def generate_meds(meds_list, used_meds, n):
    l = []
    for _ in range(n):
        u_name = random_without_repeat(meds_list, used_meds)
        name = u_name
        price = random.random() * 50 # random number between 0.0 and 50.0
        formula = u_name + ' ' + random.choice(meds_list) + ' ' + random.choice(meds_list)
        validity = random.randrange(30, 100, 10)
        query_line = """INSERT INTO MEDICINE VALUES ('""" + u_name + """', '""" + name + """', """ + str(price) + """, '""" + formula + """', """ + str(validity) + ')'
        l.append(query_line)
    return l

def generate_users(users_f_name, users_l_name, n):
    l = []
    
    # 1st user
    # user_id = 0
    user_id = 0
    supervisor_id = 0
    password = '12345'
    user_name = random.choice(users_f_name) + " " + random.choice(users_l_name)
    user_email = user_name.replace(' ', '.') + '@' + random.choice(['gmail', 'hotmail']) + '.com'
    query_line = 'EXEC dbo.spAddUser ' + str(user_id) + """, '""" + password + """', """ + str(supervisor_id) + """, '""" + user_name + """', '""" + user_email + """'"""
    l.append(query_line)

    for i in range(1, n):
        user_id = i
        supervisor_id = random.randint(0, i)
        password = '12345'
        user_name = random.choice(users_f_name) + " " + random.choice(users_l_name)
        user_email = user_name.replace(' ', '.') + '@' + random.choice(['gmail', 'hotmail']) + '.com'
        query_line = 'EXEC dbo.spAddUser ' + str(user_id) + """, '""" + password + """', """ + str(supervisor_id) + """, '""" + user_name + """', '""" + user_email + """'"""
        l.append(query_line)

    return l

def generate_pharmacies(l_names, addresses, used_pharmacies, n):
    l = []
    for _ in range(n):
        name = random.choice(l_names)
        address = random.choice(addresses)
        openning_time = str(random.randint(6, 14)) + ':' + str(random.randint(0, 59))
        closing_time = str(random.randint(int(openning_time.split(':')[0]), 23)) + ':' + str(random.randint(0, 59))
        query_line = """INSERT INTO PHARMACY VALUES ('""" + name + """', '""" + address + """', '""" + openning_time + """', '""" + closing_time + """')"""
        l.append(query_line)
        used_pharmacies.append(name)
    return l

def generate_doctors(users_f_name, users_l_name, areas, n):
    l = []
    for i in range(n):
        doctor_id = i
        doctor_name = random.choice(users_f_name) + " " + random.choice(users_l_name)
        password = 'qwerty'
        area = random.choice(areas)
        doctor_email = doctor_name.replace(' ', '.') + '@' + random.choice(['gmail', 'hotmail']) + '.com'
        doctor_phone = random_phone()
        query_line = """EXEC dbo.spAddDoctor """ + str(doctor_id) + """, '""" + password + """', '""" + doctor_name + """', '"""  + area + """', '""" + doctor_email + """', '""" + doctor_phone + """'"""
        l.append(query_line)
    return l

def generate_health_center(health_center_name, addresses, used_health_center_names, n):
    l = []
    for _ in range(n):
        name = random_without_repeat(health_center_name, used_health_center_names)
        address = random.choice(addresses)
        openning_time = str(random.randint(6, 14)) + ':' + str(random.randint(0, 59))
        closing_time = str(random.randint(int(openning_time.split(':')[0]), 23)) + ':' + str(random.randint(0, 59))
        query_line = """INSERT INTO HEALTH_CENTER VALUES ('""" + name + """', '""" + address + """', '""" + openning_time + """', '""" + closing_time + """')"""
        l.append(query_line)
    return l

def generate_appointment(patient_ids, doctor_ids, health_center_names, areas, n):
    l = []
    for i in range(n):
        appointment_id = i
        patient_id = random.randint(0, patient_ids-1)
        doctor_id = random.randint(0, doctor_ids-1)
        health_center_name = random.choice(health_center_names)
        area = random.choice(areas)
        date = str(random.randint(2017, 2019)) + '-' + str(random.randint(1, 12)) + '-' + str(random.randint(1, 30)) + ' ' + str(random.randint(6, 14)) + ':' + str(random.randint(0, 59))
        query_line = """INSERT INTO APPOINTMENT VALUES (""" + str(appointment_id) + ', ' + str(patient_id) + ', ' + str(doctor_id) + """, '""" + health_center_name + """', '""" + area + """', '""" + date + """')"""
        l.append(query_line)
    return l

def generate_prescription(appointment_ids, user_ids, pharmacy_names, n):
    l = []
    for i in range(n):
        prescription_id = i
        appointment_id = random.randint(0, appointment_ids-1)
        user_id = random.randint(0, user_ids-1)
        pharmacy_name = random.choice(pharmacy_names)
        date = str(random.randint(2017, 2019)) + '-' + str(random.randint(1, 12)) + '-' + str(random.randint(1, 30)) + ' ' + str(random.randint(6, 14)) + ':' + str(random.randint(0, 59))
        query_line = """INSERT INTO PRESCRIPTION VALUES (""" + str(prescription_id) + ', ' + str(appointment_id) + ', ' + str(user_id) + """, '""" + pharmacy_name + """', '""" + date + """')"""
        l.append(query_line)
    return l

def generate_take_medicine(user_ids, medicine_names, n):
    l = []
    for _ in range(n):
        user_id = random.randint(0, user_ids-1)
        medicine_name = random.choice(medicine_names)
        take_date = str(random.randint(2017, 2019)) + '-' + str(random.randint(1, 12)) + '-' + str(random.randint(1, 30)) + ' ' + str(random.randint(6, 14)) + ':' + str(random.randint(0, 59))
        quantity = random.randint(1, 5)
        query_line = 'INSERT INTO TAKE_MEDICINE VALUES (' + str(user_id) + """, '""" + medicine_name + """', '""" + take_date + """', """ + str(quantity) + ')'
        l.append(query_line)
    return l

def generate_stock(user_ids, medicine_names, n):
    l = []
    for _ in range(n):
        user_id = random.randint(0, user_ids-1)
        medicine_name = random.choice(medicine_names)
        expiration_date = str(random.randint(2020, 2025)) + '-' + str(random.randint(1, 12)) + '-' + str(random.randint(1, 30)) + ' ' + str(random.randint(6, 14)) + ':' + str(random.randint(0, 59))
        take_hour = str(random.randint(0, 23)) + ':' + str(random.randint(0, 59))
        take_quantity = random.randint(1, 5)
        quantity = random.randint(0, 100)
        query_line = 'EXEC dbo.spAddStock ' + str(user_id) + """, '""" + medicine_name + """', '""" + expiration_date + """', """ + str(quantity) + """, '""" + take_hour + """', """ + str(take_quantity)
        l.append(query_line)
    return l

def generate_works_on(doctor_ids, health_center_names, n):
    l = []
    for _ in range(n):
        doctor_id = random.randint(0, doctor_ids-1)
        health_center_name = random.choice(health_center_names)
        query_line = 'INSERT INTO WORKS_ON VALUES (' + str(doctor_id) + """, '""" + health_center_name + """')"""
        l.append(query_line)
    return l

def generate_medicine_prescription(prescription_ids, medicine_names, n):
    l = []
    for _ in range(n):
        prescription_id = random.randint(0, prescription_ids-1)
        medicine_name = random.choice(medicine_names)
        quantity = random.randint(0, 100)
        query_line = 'INSERT INTO MEDICINE_PRESCRIPTION VALUES (' + str(prescription_id) + """, '""" + medicine_name + """', """ + str(quantity) + ')'
        l.append(query_line)
    return l

def main(*args):  
    f_insert_meds = open('out/insert_meds.sql', 'w', encoding='utf8')
    f_insert_users = open('out/insert_users.sql', 'w', encoding='utf8')
    f_insert_pharmacies = open('out/insert_pharmacies.sql', 'w', encoding='utf8')
    f_insert_doctors = open('out/insert_doctors.sql', 'w', encoding='utf8')
    f_insert_health_centers = open('out/insert_health_centers.sql', 'w', encoding='utf8')
    f_insert_appointments = open('out/insert_appointments.sql', 'w', encoding='utf8')
    f_insert_prescriptions = open('out/insert_prescriptions.sql', 'w', encoding='utf8')
    # f_insert_take_medicines = open('out/insert_take_medicines.sql', 'w', encoding='utf8')
    f_insert_stock = open('out/insert_stock.sql', 'w', encoding='utf8')
    f_insert_works_on = open('out/insert_works_on.sql', 'w', encoding='utf8')
    f_insert_medicine_prescription = open('out/insert_medicine_prescription.sql', 'w', encoding='utf8')

    f_users_f_name = open('res/users_f_name.txt', 'r', encoding='utf8')
    f_users_l_name = open('res/users_l_name.txt', 'r', encoding='utf8')
    f_address = open('res/address.txt', 'r', encoding='utf8')
    f_health_center_name = open('res/health_center_name.txt', 'r', encoding='utf8')
    f_doctor_area = open('res/doctor_area.txt', 'r', encoding='utf8')
    f_medicine_names = open('res/medicine_names.txt', 'r', encoding='utf8')

    meds_n = 500
    users_n = 100
    pharmacies_n = 10
    doctor_n = 50
    health_center_n = 5
    appointment_n = 200
    prescription_n = 300
    # take_medicine_n = 1000
    stock_n = 500
    works_on_n = 75
    medicine_prescription_n = 500

    used_meds = []
    # users_n can work as used_user_ids
    used_pharmacies = []
    # doctor_n can work as used_doctor_ids
    used_health_center_names = []
    # appointment_n can work as used_appointment_ids
    # prescription_n can work as used_prescription_ids

    users_f_name = get_from_file(f_users_f_name)
    users_l_name = get_from_file(f_users_l_name)
    address = get_from_file(f_address)
    health_center_name = get_from_file(f_health_center_name)
    doctor_area = get_from_file(f_doctor_area)
    medicine_names = get_from_file(f_medicine_names)

    for line in generate_meds(medicine_names, used_meds, meds_n):
        f_insert_meds.write(line + '\n')

    for line in generate_users(users_f_name, users_l_name, users_n):
        f_insert_users.write(line + '\n')

    for line in generate_pharmacies(users_l_name, address, used_pharmacies, pharmacies_n):
        f_insert_pharmacies.write(line + '\n')

    for line in generate_doctors(users_f_name, users_l_name, doctor_area, doctor_n):
        f_insert_doctors.write(line + '\n')

    for line in generate_health_center(health_center_name, address, used_health_center_names, health_center_n):
        f_insert_health_centers.write(line + '\n')

    for line in generate_appointment(users_n, doctor_n, used_health_center_names, doctor_area, appointment_n):
        f_insert_appointments.write(line + '\n')

    for line in generate_prescription(appointment_n, users_n, used_pharmacies, prescription_n):
        f_insert_prescriptions.write(line + '\n')

    # for line in generate_take_medicine(users_n, used_meds, take_medicine_n):
        # f_insert_take_medicines.write(line + '\n')

    for line in generate_stock(users_n, used_meds, stock_n):
        f_insert_stock.write(line + '\n')

    for line in generate_works_on(doctor_n, used_health_center_names, works_on_n):
        f_insert_works_on.write(line + '\n')

    for line in generate_medicine_prescription(prescription_n, used_meds, medicine_prescription_n):
        f_insert_medicine_prescription.write(line + '\n')

    f_insert_meds.close()
    f_insert_meds.close()
    f_insert_meds.close()
    f_insert_meds.close()
    f_insert_meds.close()
    f_insert_meds.close()
    f_insert_users.close()
    f_insert_doctors.close()

    f_users_f_name.close()
    f_users_l_name.close()
    f_address.close()
    f_health_center_name.close()
    f_doctor_area.close()
    f_medicine_names.close()

main()
