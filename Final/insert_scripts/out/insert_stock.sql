EXEC dbo.spAddStock 85, 'Ceprotin', '2023-10-21 14:38', 88, '1:36', 4
EXEC dbo.spAddStock 77, 'Dimetane', '2022-2-25 6:15', 16, '9:43', 4
EXEC dbo.spAddStock 65, 'Bismuth Subcitrate Potassium', '2024-7-27 6:51', 81, '5:45', 1
EXEC dbo.spAddStock 94, 'Betimol', '2022-12-14 6:7', 13, '15:38', 3
EXEC dbo.spAddStock 87, 'Factor IX Complex', '2025-11-30 14:46', 57, '23:8', 5
EXEC dbo.spAddStock 11, 'Dipentum', '2025-8-24 7:31', 11, '14:28', 1
EXEC dbo.spAddStock 0, 'Coreg', '2020-3-19 12:38', 8, '7:19', 4
EXEC dbo.spAddStock 55, 'Furoxone', '2021-11-8 6:39', 39, '11:5', 2
EXEC dbo.spAddStock 46, 'Cefuroxime Axetil', '2020-7-30 9:26', 58, '2:19', 5
EXEC dbo.spAddStock 56, 'Cymbalta', '2022-2-20 11:49', 97, '10:43', 2
EXEC dbo.spAddStock 40, 'Acrivastine and Pseudoephedrine', '2024-5-24 12:39', 37, '15:31', 2
EXEC dbo.spAddStock 16, 'Eptifibatide', '2021-12-3 10:48', 14, '6:12', 4
EXEC dbo.spAddStock 0, 'Accutane', '2024-5-1 12:47', 80, '21:0', 2
EXEC dbo.spAddStock 76, 'Arranon', '2021-5-19 13:6', 72, '14:5', 2
EXEC dbo.spAddStock 86, 'Benztropine Mesylate', '2025-1-27 9:24', 19, '3:12', 3
EXEC dbo.spAddStock 77, 'Demadex', '2020-7-5 8:32', 19, '22:9', 1
EXEC dbo.spAddStock 20, 'Dostinex', '2023-7-4 7:22', 87, '9:50', 4
EXEC dbo.spAddStock 52, 'Elocon', '2020-9-27 11:56', 19, '4:17', 4
EXEC dbo.spAddStock 76, 'Felbamate', '2021-4-4 9:24', 74, '1:28', 3
EXEC dbo.spAddStock 27, 'Diclofenac Sodium', '2021-5-16 12:16', 17, '21:53', 5
EXEC dbo.spAddStock 38, 'Abatacept', '2024-10-2 11:2', 94, '0:50', 4
EXEC dbo.spAddStock 46, 'Astelin', '2025-2-18 8:18', 38, '1:16', 5
EXEC dbo.spAddStock 80, 'Eldepryl', '2020-9-10 7:26', 87, '8:57', 4
EXEC dbo.spAddStock 58, 'Ambisome', '2021-7-10 13:47', 90, '8:16', 5
EXEC dbo.spAddStock 50, 'Aldactazide', '2020-11-29 8:47', 2, '20:11', 1
EXEC dbo.spAddStock 66, 'Carisoprodol and Aspirin', '2020-4-27 9:16', 89, '18:58', 1
EXEC dbo.spAddStock 94, 'Exemestane', '2022-5-29 9:22', 10, '0:27', 5
EXEC dbo.spAddStock 33, 'Diclofenac Sodium', '2023-7-9 10:44', 1, '21:40', 5
EXEC dbo.spAddStock 19, 'Bivalirudin', '2022-6-7 6:17', 32, '11:48', 4
EXEC dbo.spAddStock 80, 'Amitriptyline', '2025-12-28 13:2', 15, '2:45', 2
EXEC dbo.spAddStock 68, 'Duramorph', '2022-7-4 8:35', 73, '22:58', 4
EXEC dbo.spAddStock 25, 'Extina', '2023-11-27 8:14', 30, '23:45', 4
EXEC dbo.spAddStock 38, 'Crofab', '2022-5-2 14:8', 24, '17:24', 4
EXEC dbo.spAddStock 48, 'Edrophonium', '2022-4-13 14:17', 64, '0:25', 2
EXEC dbo.spAddStock 36, 'Ceftriaxone', '2024-1-22 11:10', 13, '22:42', 3
EXEC dbo.spAddStock 89, 'Curosurf', '2024-2-13 6:27', 4, '4:22', 2
EXEC dbo.spAddStock 94, 'Erythromycin Ethylsuccinate', '2025-9-17 13:15', 36, '2:41', 3
EXEC dbo.spAddStock 50, 'Aldesleukin', '2021-3-20 11:44', 18, '10:25', 5
EXEC dbo.spAddStock 31, 'Dynapen', '2025-7-26 14:49', 16, '14:55', 1
EXEC dbo.spAddStock 38, 'FML Forte', '2025-5-1 6:18', 38, '11:36', 2
EXEC dbo.spAddStock 22, 'Dipentum', '2020-9-7 13:20', 48, '0:4', 3
EXEC dbo.spAddStock 47, 'Deferoxamine', '2022-7-27 9:32', 95, '12:3', 2
EXEC dbo.spAddStock 64, 'Esomeprazole Magnesium', '2023-6-6 14:19', 23, '1:36', 2
EXEC dbo.spAddStock 50, 'Desogen', '2024-6-17 6:52', 95, '5:10', 2
EXEC dbo.spAddStock 25, 'Definity', '2020-8-23 6:39', 87, '13:51', 2
EXEC dbo.spAddStock 17, 'Astelin', '2023-7-11 7:22', 25, '11:39', 5
EXEC dbo.spAddStock 24, 'Calcium Disodium Versenate', '2020-9-10 6:22', 8, '8:51', 3
EXEC dbo.spAddStock 58, 'Compro', '2023-9-27 6:10', 69, '8:45', 2
EXEC dbo.spAddStock 13, 'Benazepril', '2021-12-12 8:10', 0, '1:5', 4
EXEC dbo.spAddStock 2, 'Cinobac', '2024-10-20 9:23', 10, '6:35', 3
EXEC dbo.spAddStock 13, 'Flumadine', '2020-7-27 14:49', 60, '1:24', 3
EXEC dbo.spAddStock 10, 'Bretylium', '2020-3-29 13:45', 7, '7:44', 4
EXEC dbo.spAddStock 54, 'Climara Pro', '2021-2-24 11:5', 100, '9:44', 4
EXEC dbo.spAddStock 18, 'Ephedrine', '2021-10-11 14:24', 67, '4:54', 2
EXEC dbo.spAddStock 91, 'Bextra', '2024-1-5 6:26', 84, '23:14', 1
EXEC dbo.spAddStock 95, 'Ancobon', '2024-5-2 12:14', 85, '8:53', 1
EXEC dbo.spAddStock 95, 'Actos', '2020-3-4 6:23', 82, '15:8', 3
EXEC dbo.spAddStock 81, 'Cellulose', '2022-3-26 7:32', 5, '21:29', 2
EXEC dbo.spAddStock 0, 'Dopar', '2024-8-21 13:54', 89, '21:4', 5
EXEC dbo.spAddStock 15, 'Azasite', '2023-3-14 13:17', 30, '23:47', 5
EXEC dbo.spAddStock 17, 'Eplerenone', '2022-5-5 9:0', 91, '7:42', 3
EXEC dbo.spAddStock 95, 'Doryx', '2020-12-23 11:46', 4, '6:25', 1
EXEC dbo.spAddStock 48, 'Estramustine', '2024-9-8 13:7', 94, '3:8', 3
EXEC dbo.spAddStock 11, 'Anturane', '2023-11-25 6:7', 12, '22:19', 3
EXEC dbo.spAddStock 57, 'Capastat Sulfate', '2023-8-12 12:50', 58, '7:24', 5
EXEC dbo.spAddStock 23, 'Cinoxacin', '2021-1-20 11:41', 63, '9:6', 2
EXEC dbo.spAddStock 24, 'Bretylium', '2021-4-5 8:27', 67, '23:41', 4
EXEC dbo.spAddStock 61, 'Comtan', '2023-4-5 14:13', 32, '11:8', 2
EXEC dbo.spAddStock 66, 'Delestrogen', '2021-5-25 12:17', 55, '19:16', 5
EXEC dbo.spAddStock 87, 'Cholera Vaccine', '2021-10-13 11:9', 82, '2:10', 5
EXEC dbo.spAddStock 64, 'Estring', '2022-9-18 11:3', 62, '19:53', 5
EXEC dbo.spAddStock 73, 'Alefacept', '2021-5-8 9:0', 66, '18:41', 2
EXEC dbo.spAddStock 61, 'Cloderm', '2025-5-3 7:18', 11, '0:17', 4
EXEC dbo.spAddStock 19, 'Cellulose', '2020-4-5 11:34', 68, '22:43', 1
EXEC dbo.spAddStock 66, 'Alefacept', '2020-10-28 8:26', 38, '16:11', 4
EXEC dbo.spAddStock 11, 'Anusol Hc', '2020-12-12 12:21', 54, '17:54', 3
EXEC dbo.spAddStock 5, 'Diflucan', '2023-1-27 14:30', 37, '22:48', 1
EXEC dbo.spAddStock 54, 'Follitropin Beta', '2021-7-15 7:53', 65, '20:3', 5
EXEC dbo.spAddStock 69, 'Compazine', '2024-1-21 13:53', 12, '4:27', 1
EXEC dbo.spAddStock 49, 'Cerezyme', '2025-6-13 9:53', 53, '10:42', 5
EXEC dbo.spAddStock 46, 'Dipentum', '2024-1-5 6:23', 68, '15:11', 5
EXEC dbo.spAddStock 96, 'Fenofibric Acid', '2020-4-10 10:40', 81, '19:54', 3
EXEC dbo.spAddStock 8, 'Fentanyl Buccal', '2023-5-10 8:31', 15, '13:2', 1
EXEC dbo.spAddStock 62, 'Emsam', '2024-11-9 6:27', 83, '6:8', 3
EXEC dbo.spAddStock 60, 'Coly-Mycin M', '2021-12-16 12:5', 96, '23:4', 5
EXEC dbo.spAddStock 87, 'Antithrombin', '2025-3-10 13:22', 89, '5:50', 4
EXEC dbo.spAddStock 63, 'Aldactazide', '2022-5-1 12:26', 81, '0:49', 1
EXEC dbo.spAddStock 97, 'Duricef', '2024-4-28 7:28', 38, '8:49', 4
EXEC dbo.spAddStock 60, 'Dalteparin', '2025-3-24 14:8', 4, '4:55', 4
EXEC dbo.spAddStock 58, 'Detrol', '2021-4-15 6:1', 20, '10:45', 5
EXEC dbo.spAddStock 18, 'Exjade', '2021-3-24 6:2', 10, '10:11', 4
EXEC dbo.spAddStock 10, 'Amytal Sodium', '2020-8-1 13:4', 72, '20:16', 1
EXEC dbo.spAddStock 69, 'Cefuroxime Axetil', '2025-11-6 6:39', 43, '13:8', 3
EXEC dbo.spAddStock 42, 'Flutamide', '2023-6-2 10:16', 35, '22:37', 5
EXEC dbo.spAddStock 45, 'Comtan', '2024-11-20 14:29', 40, '13:18', 4
EXEC dbo.spAddStock 5, 'Antizol', '2024-11-15 13:26', 39, '9:55', 3
EXEC dbo.spAddStock 78, 'Estazolam', '2023-12-2 9:3', 83, '14:27', 4
EXEC dbo.spAddStock 88, 'Aminoglutethimide', '2021-8-8 11:28', 90, '19:39', 2
EXEC dbo.spAddStock 23, 'Aricept', '2024-7-6 10:6', 11, '10:38', 2
EXEC dbo.spAddStock 64, 'Coartem', '2024-3-24 13:24', 38, '17:52', 1
EXEC dbo.spAddStock 85, 'Aptivus', '2022-6-20 14:28', 60, '14:53', 5
EXEC dbo.spAddStock 62, 'Adderall', '2022-7-22 9:20', 23, '4:15', 4
EXEC dbo.spAddStock 46, 'Aptivus', '2023-11-7 6:2', 96, '18:58', 2
EXEC dbo.spAddStock 6, 'Ezetimibe and Simvastatin', '2024-4-9 12:18', 55, '0:21', 4
EXEC dbo.spAddStock 95, 'Dynacirc', '2021-11-11 13:44', 68, '9:50', 2
EXEC dbo.spAddStock 36, 'Amiloride', '2021-10-2 6:11', 4, '6:40', 3
EXEC dbo.spAddStock 46, 'Artane', '2020-8-13 6:28', 22, '6:21', 2
EXEC dbo.spAddStock 38, 'Epipen', '2024-10-8 6:5', 73, '17:52', 2
EXEC dbo.spAddStock 81, 'Bactroban Nasal', '2022-5-17 12:57', 53, '22:29', 5
EXEC dbo.spAddStock 14, 'Angeliq', '2022-2-11 12:23', 8, '4:36', 1
EXEC dbo.spAddStock 6, 'Anturane', '2021-3-13 13:19', 9, '10:41', 5
EXEC dbo.spAddStock 9, 'Enoxacin', '2024-8-24 12:20', 79, '20:33', 1
EXEC dbo.spAddStock 47, 'Cancidas', '2025-6-27 13:6', 79, '23:15', 4
EXEC dbo.spAddStock 29, 'Chibroxin', '2025-9-25 10:4', 64, '12:35', 4
EXEC dbo.spAddStock 99, 'Fiorinal with Codeine', '2025-5-9 6:5', 48, '7:25', 5
EXEC dbo.spAddStock 17, 'Axert', '2021-7-2 7:32', 20, '9:24', 4
EXEC dbo.spAddStock 59, 'Alamast', '2020-2-8 8:33', 2, '0:13', 2
EXEC dbo.spAddStock 82, 'Atripla', '2020-9-11 13:37', 79, '7:47', 1
EXEC dbo.spAddStock 30, 'Doxapram', '2025-6-23 13:24', 15, '22:32', 5
EXEC dbo.spAddStock 16, 'Carisoprodol and Aspirin', '2024-6-29 14:55', 73, '5:43', 3
EXEC dbo.spAddStock 72, 'Aldesleukin', '2021-4-27 9:38', 14, '18:7', 4
EXEC dbo.spAddStock 51, 'Dacarbazine', '2020-7-29 7:34', 36, '20:41', 1
EXEC dbo.spAddStock 21, 'Clotrimazole', '2023-5-30 12:1', 40, '22:1', 3
EXEC dbo.spAddStock 75, 'Alfenta', '2021-4-14 7:8', 34, '21:14', 4
EXEC dbo.spAddStock 95, 'Ephedrine', '2022-5-6 9:35', 6, '13:7', 5
EXEC dbo.spAddStock 16, 'Benztropine Mesylate', '2023-1-8 6:26', 61, '22:38', 3
EXEC dbo.spAddStock 41, 'Duricef', '2021-6-5 14:11', 44, '22:5', 4
EXEC dbo.spAddStock 26, 'Foscarnet Sodium', '2023-8-5 8:39', 1, '0:51', 5
EXEC dbo.spAddStock 11, 'Dapsone', '2020-10-19 9:13', 92, '6:51', 4
EXEC dbo.spAddStock 78, 'Fenofibric Acid', '2022-6-3 12:19', 48, '14:28', 3
EXEC dbo.spAddStock 87, 'Cerezyme', '2020-12-14 8:8', 53, '22:55', 2
EXEC dbo.spAddStock 41, 'Effexor', '2020-12-25 7:27', 100, '23:4', 3
EXEC dbo.spAddStock 98, 'Cyklokapron', '2023-7-10 13:42', 13, '11:52', 5
EXEC dbo.spAddStock 76, 'Etoposide Phosphate', '2021-11-23 8:12', 65, '15:8', 1
EXEC dbo.spAddStock 96, 'Flecainide', '2020-12-27 9:49', 35, '20:58', 5
EXEC dbo.spAddStock 68, 'Fenoldopam Mesylate', '2020-11-17 14:9', 42, '18:32', 4
EXEC dbo.spAddStock 89, 'Aceon', '2023-12-25 7:48', 44, '18:36', 2
EXEC dbo.spAddStock 25, 'Arranon', '2021-1-12 8:17', 66, '12:25', 4
EXEC dbo.spAddStock 53, 'Actemra', '2025-3-18 13:45', 51, '18:35', 2
EXEC dbo.spAddStock 3, 'Adderall', '2021-7-14 7:9', 17, '3:48', 2
EXEC dbo.spAddStock 11, 'Allopurinol', '2021-5-19 12:7', 28, '23:40', 4
EXEC dbo.spAddStock 64, 'Cancidas', '2022-8-15 7:39', 88, '1:50', 1
EXEC dbo.spAddStock 14, 'Aldesleukin', '2025-3-8 8:13', 38, '7:0', 2
EXEC dbo.spAddStock 84, 'Diltiazem Hcl', '2023-11-15 13:33', 61, '18:17', 5
EXEC dbo.spAddStock 64, 'Cialis', '2020-3-5 7:41', 67, '1:53', 3
EXEC dbo.spAddStock 73, 'Floxin Otic', '2024-6-3 9:4', 6, '18:43', 2
EXEC dbo.spAddStock 95, 'Elocon', '2025-2-21 7:7', 90, '13:43', 5
EXEC dbo.spAddStock 11, 'Cortenema', '2025-12-10 7:51', 48, '3:44', 3
EXEC dbo.spAddStock 35, 'Covera-HS', '2025-5-1 12:8', 92, '18:13', 3
EXEC dbo.spAddStock 53, 'Coly-Mycin M', '2022-1-1 7:46', 23, '2:18', 2
EXEC dbo.spAddStock 25, 'Aldurazyme', '2020-9-30 10:46', 23, '23:39', 4
EXEC dbo.spAddStock 97, 'Doxercalciferol', '2024-6-26 11:27', 72, '19:29', 2
EXEC dbo.spAddStock 89, 'Clobetasol Propionate Gel', '2025-9-18 6:32', 15, '12:26', 4
EXEC dbo.spAddStock 31, 'Candesartan Cilexetil', '2025-6-8 14:19', 14, '20:19', 3
EXEC dbo.spAddStock 79, 'Ellence', '2025-7-2 13:30', 60, '20:37', 3
EXEC dbo.spAddStock 82, 'Butenafine', '2025-11-26 6:43', 16, '5:35', 5
EXEC dbo.spAddStock 71, 'Erythromycin Ethylsuccinate', '2024-3-10 12:53', 52, '20:15', 2
EXEC dbo.spAddStock 11, 'Avelox', '2023-2-28 6:55', 82, '11:29', 5
EXEC dbo.spAddStock 35, 'Antara', '2024-9-4 8:16', 64, '22:10', 3
EXEC dbo.spAddStock 18, 'Clocortolone', '2020-7-20 8:1', 88, '7:26', 2
EXEC dbo.spAddStock 15, 'Combigan', '2024-3-13 9:8', 99, '13:14', 5
EXEC dbo.spAddStock 0, 'Cefizox', '2024-11-22 14:58', 78, '9:36', 4
EXEC dbo.spAddStock 10, 'Antizol', '2024-11-23 10:29', 84, '19:3', 4
EXEC dbo.spAddStock 72, 'Climara', '2022-5-26 6:51', 99, '23:34', 3
EXEC dbo.spAddStock 18, 'Ceretec', '2025-2-15 8:16', 41, '14:56', 2
EXEC dbo.spAddStock 68, 'Dihydroergotamine', '2024-6-3 14:50', 37, '15:22', 2
EXEC dbo.spAddStock 4, 'Frovatriptan Succinate', '2022-3-1 13:38', 48, '3:48', 3
EXEC dbo.spAddStock 40, 'Adderall', '2022-12-8 9:50', 74, '6:45', 1
EXEC dbo.spAddStock 32, 'Foscavir', '2020-5-15 8:38', 13, '14:4', 4
EXEC dbo.spAddStock 47, 'Coartem', '2023-10-3 14:39', 46, '0:14', 2
EXEC dbo.spAddStock 2, 'Dtic-Dome', '2021-2-12 14:18', 80, '2:34', 1
EXEC dbo.spAddStock 77, 'Dofetilide', '2023-4-13 6:52', 53, '18:38', 5
EXEC dbo.spAddStock 83, 'Amitriptyline', '2022-11-13 12:13', 0, '15:36', 4
EXEC dbo.spAddStock 11, 'Fluoroplex', '2021-4-29 7:0', 94, '15:32', 2
EXEC dbo.spAddStock 3, 'Cyclessa', '2025-6-20 11:14', 66, '17:17', 5
EXEC dbo.spAddStock 56, 'Anturane', '2021-3-20 14:23', 6, '1:10', 1
EXEC dbo.spAddStock 15, 'Cardene SR', '2023-2-1 12:31', 86, '23:15', 5
EXEC dbo.spAddStock 68, 'Alrex', '2025-7-2 10:10', 79, '9:53', 1
EXEC dbo.spAddStock 79, 'Diltiazem Hcl', '2022-4-7 6:13', 21, '19:42', 2
EXEC dbo.spAddStock 21, 'Etrafon', '2023-3-23 12:5', 28, '0:30', 4
EXEC dbo.spAddStock 61, 'Adoxa', '2025-12-3 10:18', 43, '20:23', 2
EXEC dbo.spAddStock 92, 'Estropipate', '2023-9-7 11:1', 26, '7:29', 4
EXEC dbo.spAddStock 99, 'Finasteride', '2022-2-17 14:56', 98, '18:13', 3
EXEC dbo.spAddStock 49, 'Clofazimine', '2020-2-3 13:46', 68, '18:46', 4
EXEC dbo.spAddStock 97, 'Epogen', '2024-1-15 14:1', 51, '19:52', 4
EXEC dbo.spAddStock 89, 'Aricept', '2020-1-24 9:48', 62, '20:14', 5
EXEC dbo.spAddStock 22, 'Cetirizine', '2025-8-4 9:23', 89, '16:22', 1
EXEC dbo.spAddStock 88, 'Embeda', '2025-12-24 14:23', 100, '14:33', 2
EXEC dbo.spAddStock 32, 'Cefobid', '2025-7-19 14:11', 90, '6:43', 5
EXEC dbo.spAddStock 60, 'Actimmune', '2024-3-16 10:8', 53, '20:33', 1
EXEC dbo.spAddStock 41, 'Bicillin C-R 900/300', '2021-5-19 13:17', 86, '1:59', 3
EXEC dbo.spAddStock 88, 'Alomide', '2023-7-15 13:12', 78, '14:29', 5
EXEC dbo.spAddStock 69, 'Flavoxate HCl', '2025-10-25 14:1', 5, '20:35', 2
EXEC dbo.spAddStock 52, 'Ceredase', '2025-12-29 12:6', 25, '1:18', 3
EXEC dbo.spAddStock 81, 'Diazepam', '2025-10-18 14:37', 91, '3:38', 4
EXEC dbo.spAddStock 21, 'Cardene SR', '2020-9-7 7:26', 97, '19:36', 4
EXEC dbo.spAddStock 0, 'Cytotec', '2024-9-18 9:52', 97, '22:43', 1
EXEC dbo.spAddStock 40, 'Diflucan', '2020-10-13 14:15', 94, '5:1', 4
EXEC dbo.spAddStock 4, 'Apokyn', '2020-12-1 9:53', 3, '22:57', 4
EXEC dbo.spAddStock 35, 'Citalopram Hydrobromide', '2021-6-27 7:5', 18, '3:30', 5
EXEC dbo.spAddStock 61, 'Edrophonium', '2022-6-12 6:25', 75, '10:44', 2
EXEC dbo.spAddStock 77, 'Estrostep 21', '2020-2-16 13:7', 32, '15:43', 4
EXEC dbo.spAddStock 92, 'Cytovene', '2024-1-1 10:46', 25, '3:50', 1
EXEC dbo.spAddStock 35, 'Compazine', '2023-2-22 10:6', 66, '10:10', 5
EXEC dbo.spAddStock 21, 'Amitriptyline', '2024-2-11 6:15', 76, '1:43', 2
EXEC dbo.spAddStock 87, 'Dimetane', '2024-12-17 9:33', 76, '23:38', 1
EXEC dbo.spAddStock 51, 'Emend', '2025-5-25 12:0', 4, '22:58', 3
EXEC dbo.spAddStock 90, 'Ephedrine', '2024-3-19 9:45', 19, '11:11', 2
EXEC dbo.spAddStock 28, 'Calcium Disodium Versenate', '2021-4-1 10:29', 68, '2:20', 4
EXEC dbo.spAddStock 4, 'Fluoxymesterone', '2023-6-23 12:50', 66, '11:29', 1
EXEC dbo.spAddStock 47, 'Depacon', '2022-10-25 12:4', 57, '7:8', 3
EXEC dbo.spAddStock 2, 'Exemestane', '2021-6-13 8:46', 5, '12:9', 3
EXEC dbo.spAddStock 15, 'Duramorph', '2022-11-18 14:31', 77, '5:10', 4
EXEC dbo.spAddStock 97, 'Acamprosate Calcium', '2025-2-8 9:2', 23, '14:12', 1
EXEC dbo.spAddStock 59, 'Diltiazem', '2021-5-30 9:28', 45, '19:16', 5
EXEC dbo.spAddStock 35, 'Antizol', '2021-7-15 12:8', 29, '22:24', 5
EXEC dbo.spAddStock 25, 'Erythromycin Ethylsuccinate', '2025-5-26 6:18', 46, '21:41', 5
EXEC dbo.spAddStock 21, 'Ancobon', '2025-4-7 13:53', 57, '3:55', 3
EXEC dbo.spAddStock 41, 'Duricef', '2025-12-27 6:58', 90, '13:3', 3
EXEC dbo.spAddStock 91, 'Definity', '2025-2-25 12:15', 82, '17:35', 4
EXEC dbo.spAddStock 14, 'Almotriptan Malate', '2025-5-28 8:12', 11, '17:38', 5
EXEC dbo.spAddStock 30, 'Etopophos', '2024-10-13 11:44', 17, '7:55', 3
EXEC dbo.spAddStock 6, 'Flumadine', '2022-10-2 13:58', 58, '1:58', 4
EXEC dbo.spAddStock 15, 'Dalteparin', '2023-3-30 7:37', 80, '7:43', 5
EXEC dbo.spAddStock 10, 'Acetic Acid', '2023-6-30 6:19', 4, '1:37', 5
EXEC dbo.spAddStock 1, 'Creon', '2020-5-17 10:35', 26, '5:43', 1
EXEC dbo.spAddStock 39, 'Actiq', '2025-11-14 11:25', 81, '3:50', 4
EXEC dbo.spAddStock 39, 'Bicillin C-R 900/300', '2020-9-2 14:6', 5, '3:33', 1
EXEC dbo.spAddStock 11, 'Bromocriptine Mesylate', '2024-1-24 10:22', 60, '16:31', 4
EXEC dbo.spAddStock 21, 'Bretylium', '2020-2-14 14:34', 4, '0:57', 1
EXEC dbo.spAddStock 63, 'Ampyra', '2022-12-4 14:1', 67, '5:59', 5
EXEC dbo.spAddStock 21, 'Calciferol', '2021-3-28 6:20', 40, '19:31', 5
EXEC dbo.spAddStock 0, 'Acamprosate Calcium', '2022-8-29 10:16', 97, '22:40', 4
EXEC dbo.spAddStock 48, 'Arcalyst', '2020-10-30 11:45', 32, '21:30', 4
EXEC dbo.spAddStock 34, 'Cefobid', '2022-8-16 8:28', 44, '0:22', 3
EXEC dbo.spAddStock 81, 'Cognex', '2022-4-6 10:12', 78, '7:25', 4
EXEC dbo.spAddStock 27, 'Diflucan', '2022-11-25 6:15', 88, '6:0', 4
EXEC dbo.spAddStock 4, 'Angeliq', '2020-12-12 14:13', 8, '23:48', 4
EXEC dbo.spAddStock 26, 'Dihydroergotamine', '2025-8-7 8:43', 36, '17:50', 2
EXEC dbo.spAddStock 52, 'Candesartan Cilexetil', '2022-5-27 12:23', 0, '10:52', 2
EXEC dbo.spAddStock 85, 'Dimetane', '2024-6-11 8:57', 87, '7:55', 3
EXEC dbo.spAddStock 73, 'Flavoxate HCl', '2025-4-26 8:25', 75, '19:34', 3
EXEC dbo.spAddStock 47, 'Adoxa', '2025-11-27 7:53', 26, '20:24', 2
EXEC dbo.spAddStock 7, 'Cetirizine', '2024-2-8 6:4', 21, '6:9', 1
EXEC dbo.spAddStock 57, 'Fentanyl Buccal', '2024-3-8 12:29', 95, '6:4', 1
EXEC dbo.spAddStock 93, 'Armodafinil', '2021-6-7 7:8', 65, '9:40', 2
EXEC dbo.spAddStock 42, 'Dihydroergotamine', '2024-11-26 14:43', 68, '0:15', 2
EXEC dbo.spAddStock 61, 'Equetro', '2020-12-29 10:51', 76, '12:6', 2
EXEC dbo.spAddStock 34, 'Fosaprepitant Dimeglumine', '2020-1-10 11:46', 74, '11:44', 5
EXEC dbo.spAddStock 96, 'Ethrane', '2023-2-16 14:34', 79, '22:19', 2
EXEC dbo.spAddStock 34, 'Calciferol', '2020-5-10 7:8', 29, '22:53', 5
EXEC dbo.spAddStock 54, 'Alclometasone Dipropionate', '2025-11-3 13:58', 53, '9:1', 3
EXEC dbo.spAddStock 11, 'Coartem', '2024-10-3 6:22', 0, '2:11', 2
EXEC dbo.spAddStock 15, 'Cytovene', '2024-7-21 6:43', 48, '0:50', 2
EXEC dbo.spAddStock 26, 'Dynapen', '2021-11-26 6:21', 19, '22:20', 4
EXEC dbo.spAddStock 17, 'Dopamine Hydrochloride', '2023-5-30 13:35', 66, '4:19', 3
EXEC dbo.spAddStock 3, 'Clomid', '2020-12-2 7:6', 82, '2:26', 1
EXEC dbo.spAddStock 72, 'Cefadroxil Hemihydrate', '2021-2-13 11:7', 84, '0:59', 3
EXEC dbo.spAddStock 56, 'Chloramphenicol', '2023-6-9 9:20', 4, '23:2', 1
EXEC dbo.spAddStock 33, 'Anidulafungin', '2020-7-17 8:37', 84, '18:49', 2
EXEC dbo.spAddStock 73, 'Fluoxymesterone', '2020-7-22 6:53', 12, '13:30', 3
EXEC dbo.spAddStock 45, 'Atripla', '2021-6-21 13:24', 59, '22:29', 1
EXEC dbo.spAddStock 76, 'Exemestane', '2025-4-10 8:4', 22, '5:16', 1
EXEC dbo.spAddStock 85, 'Cafergot', '2024-9-24 8:38', 89, '16:35', 1
EXEC dbo.spAddStock 16, 'Acticin', '2023-2-11 13:7', 14, '10:28', 2
EXEC dbo.spAddStock 13, 'Amoxicillin', '2022-10-30 9:15', 65, '18:47', 2
EXEC dbo.spAddStock 62, 'Darunavir', '2020-11-7 9:15', 5, '13:3', 1
EXEC dbo.spAddStock 55, 'Dynapen', '2020-9-25 13:50', 64, '3:21', 3
EXEC dbo.spAddStock 48, 'Elavil', '2025-5-1 14:19', 45, '15:24', 5
EXEC dbo.spAddStock 7, 'Alimta', '2024-7-7 13:42', 65, '14:35', 5
EXEC dbo.spAddStock 82, 'Acyclovir', '2023-11-18 14:27', 34, '23:45', 1
EXEC dbo.spAddStock 90, 'Ceftin', '2025-10-6 6:57', 9, '1:4', 4
EXEC dbo.spAddStock 69, 'Dopar', '2022-2-21 10:11', 5, '8:13', 1
EXEC dbo.spAddStock 71, 'Alli', '2022-8-14 9:25', 94, '14:5', 1
EXEC dbo.spAddStock 73, 'Cinoxacin', '2023-12-18 10:21', 71, '23:55', 3
EXEC dbo.spAddStock 9, 'Avelox', '2021-4-24 11:53', 44, '18:16', 1
EXEC dbo.spAddStock 84, 'Actemra', '2022-1-17 12:29', 18, '20:53', 3
EXEC dbo.spAddStock 20, 'Cefizox', '2021-10-7 9:39', 44, '1:57', 4
EXEC dbo.spAddStock 62, 'Combunox', '2024-9-25 8:9', 58, '0:47', 3
EXEC dbo.spAddStock 96, 'Asparaginase', '2023-8-6 13:52', 65, '23:51', 3
EXEC dbo.spAddStock 85, 'Acticin', '2024-3-22 7:17', 48, '23:41', 3
EXEC dbo.spAddStock 4, 'Fentanyl Buccal', '2024-8-11 6:29', 73, '1:54', 1
EXEC dbo.spAddStock 10, 'Fentanyl Citrate', '2021-9-29 10:43', 19, '23:38', 5
EXEC dbo.spAddStock 57, 'Cytovene', '2021-9-23 10:56', 49, '4:10', 5
EXEC dbo.spAddStock 47, 'Epiduo', '2024-1-19 9:4', 2, '3:4', 4
EXEC dbo.spAddStock 50, 'Antizol', '2020-10-6 13:45', 43, '8:11', 5
EXEC dbo.spAddStock 84, 'Flolan', '2024-7-6 7:29', 28, '8:23', 4
EXEC dbo.spAddStock 79, 'Cymbalta', '2021-10-6 11:20', 20, '8:9', 1
EXEC dbo.spAddStock 67, 'Cyclessa', '2021-3-23 12:22', 2, '8:5', 3
EXEC dbo.spAddStock 56, 'Cetrorelix', '2021-3-27 7:1', 11, '19:3', 1
EXEC dbo.spAddStock 39, 'Diethylpropion', '2025-3-29 13:52', 35, '23:37', 2
EXEC dbo.spAddStock 36, 'Dolasetron', '2020-10-12 8:27', 84, '8:38', 4
EXEC dbo.spAddStock 12, 'Betaseron', '2020-11-8 11:22', 67, '3:35', 4
EXEC dbo.spAddStock 87, 'Clobetasol Propionate', '2020-9-4 10:43', 2, '11:30', 2
EXEC dbo.spAddStock 88, 'Fortaz', '2023-5-7 14:47', 85, '21:37', 5
EXEC dbo.spAddStock 6, 'Cysteamine Bitartrate', '2023-12-27 6:5', 61, '17:55', 2
EXEC dbo.spAddStock 19, 'Cesamet', '2023-5-27 11:7', 9, '8:1', 2
EXEC dbo.spAddStock 5, 'Allopurinol', '2024-9-28 7:12', 96, '5:59', 1
EXEC dbo.spAddStock 47, 'Betaxon', '2022-6-18 14:18', 72, '7:0', 2
EXEC dbo.spAddStock 48, 'Acticin', '2022-10-7 7:44', 46, '5:47', 4
EXEC dbo.spAddStock 29, 'Frovatriptan Succinate', '2021-11-26 11:42', 55, '14:41', 1
EXEC dbo.spAddStock 40, 'Dostinex', '2023-2-7 14:38', 90, '10:8', 3
EXEC dbo.spAddStock 87, 'Actiq', '2025-8-11 11:41', 16, '20:27', 2
EXEC dbo.spAddStock 69, 'Cortisone Acetate', '2021-3-30 10:36', 96, '10:2', 5
EXEC dbo.spAddStock 51, 'Actos', '2025-12-15 7:38', 5, '1:10', 1
EXEC dbo.spAddStock 47, 'Adderall', '2021-5-3 10:17', 89, '18:55', 4
EXEC dbo.spAddStock 0, 'Flutamide', '2023-4-4 11:43', 26, '12:13', 1
EXEC dbo.spAddStock 37, 'Alphanate', '2025-6-28 8:49', 24, '20:41', 4
EXEC dbo.spAddStock 12, 'Cleocin', '2023-12-9 10:44', 36, '1:55', 1
EXEC dbo.spAddStock 17, 'Felbamate', '2020-4-20 9:42', 97, '11:45', 2
EXEC dbo.spAddStock 25, 'Dopar', '2021-4-7 7:26', 34, '19:40', 1
EXEC dbo.spAddStock 93, 'Fludeoxyglucose F 18', '2021-4-11 12:5', 76, '7:56', 4
EXEC dbo.spAddStock 74, 'Coly-Mycin M', '2020-3-5 12:50', 70, '6:51', 2
EXEC dbo.spAddStock 22, 'Arranon', '2024-6-10 9:16', 7, '21:46', 4
EXEC dbo.spAddStock 25, 'Calciferol', '2024-11-15 9:12', 13, '18:24', 4
EXEC dbo.spAddStock 4, 'Antizol', '2025-8-27 14:18', 77, '11:23', 5
EXEC dbo.spAddStock 63, 'Apokyn', '2024-12-9 14:47', 79, '7:6', 1
EXEC dbo.spAddStock 14, 'Felbamate', '2023-1-28 6:2', 48, '13:49', 5
EXEC dbo.spAddStock 35, 'Atenolol and Chlorthalidone', '2020-4-23 7:40', 67, '20:31', 5
EXEC dbo.spAddStock 54, 'Etopophos', '2024-10-17 12:14', 95, '14:7', 1
EXEC dbo.spAddStock 64, 'Capastat Sulfate', '2022-1-1 13:31', 0, '8:21', 5
EXEC dbo.spAddStock 67, 'Acticin', '2023-3-23 12:55', 100, '18:35', 4
EXEC dbo.spAddStock 31, 'Desloratadine', '2020-5-7 9:49', 79, '4:19', 2
EXEC dbo.spAddStock 20, 'Ethacrynic Acid', '2021-12-22 10:56', 93, '21:29', 4
EXEC dbo.spAddStock 56, 'Duramorph', '2025-9-6 6:15', 14, '4:54', 2
EXEC dbo.spAddStock 71, 'Emsam', '2020-10-23 9:13', 4, '5:26', 2
EXEC dbo.spAddStock 99, 'Bayer', '2020-10-9 6:26', 49, '1:3', 3
EXEC dbo.spAddStock 0, 'Flomax', '2020-5-14 11:23', 67, '21:15', 3
EXEC dbo.spAddStock 15, 'Altabax', '2024-1-26 11:0', 27, '19:16', 1
EXEC dbo.spAddStock 31, 'Amitiza', '2022-6-22 8:13', 70, '22:15', 1
EXEC dbo.spAddStock 31, 'Adoxa', '2020-6-13 13:52', 9, '15:11', 3
EXEC dbo.spAddStock 32, 'Dipentum', '2021-4-7 8:46', 78, '12:24', 1
EXEC dbo.spAddStock 57, 'Aldesleukin', '2024-8-17 12:1', 25, '14:54', 1
EXEC dbo.spAddStock 89, 'Estropipate', '2024-6-15 7:53', 53, '1:0', 1
EXEC dbo.spAddStock 2, 'Emend', '2020-9-18 12:33', 82, '16:19', 3
EXEC dbo.spAddStock 60, 'Chlorpheniramine Maleate', '2020-4-3 6:12', 16, '1:21', 2
EXEC dbo.spAddStock 25, 'Alphanate', '2020-10-30 13:8', 77, '16:11', 5
EXEC dbo.spAddStock 60, 'Elitek', '2022-9-1 14:41', 72, '8:0', 1
EXEC dbo.spAddStock 30, 'Betapace', '2024-6-17 14:11', 45, '16:19', 4
EXEC dbo.spAddStock 28, 'Amlodipine Besylate', '2024-9-6 10:41', 96, '12:17', 4
EXEC dbo.spAddStock 83, 'Adderall', '2024-9-8 7:41', 94, '4:3', 3
EXEC dbo.spAddStock 85, 'Accutane', '2024-6-10 14:52', 96, '23:24', 5
EXEC dbo.spAddStock 64, 'Benzphetamine', '2022-1-13 8:3', 10, '19:33', 3
EXEC dbo.spAddStock 86, 'Dexmedetomidine hydrochloride', '2025-5-13 9:59', 97, '23:33', 2
EXEC dbo.spAddStock 45, 'Elidel', '2025-10-6 9:40', 45, '20:43', 2
EXEC dbo.spAddStock 92, 'Fluocinonide', '2025-10-21 9:27', 13, '13:34', 3
EXEC dbo.spAddStock 87, 'Canasa', '2024-2-28 13:14', 80, '14:11', 1
EXEC dbo.spAddStock 52, 'Coly-Mycin M', '2020-4-5 11:3', 67, '6:43', 4
EXEC dbo.spAddStock 68, 'Duricef', '2023-1-3 8:57', 10, '1:17', 3
EXEC dbo.spAddStock 68, 'Benicar HCT', '2021-6-8 12:44', 16, '1:30', 2
EXEC dbo.spAddStock 92, 'Essential Amino Acids', '2025-5-26 14:13', 70, '23:14', 5
EXEC dbo.spAddStock 35, 'Calan', '2022-5-29 10:24', 40, '5:15', 5
EXEC dbo.spAddStock 42, 'Cedax', '2022-12-19 6:40', 7, '17:47', 1
EXEC dbo.spAddStock 94, 'Aldurazyme', '2025-11-13 9:15', 39, '21:12', 5
EXEC dbo.spAddStock 82, 'Betapace', '2023-3-17 9:35', 13, '14:28', 2
EXEC dbo.spAddStock 41, 'Aldurazyme', '2022-6-18 14:36', 26, '7:21', 2
EXEC dbo.spAddStock 38, 'AccuNeb', '2021-3-5 7:49', 26, '21:37', 5
EXEC dbo.spAddStock 67, 'Extavia', '2021-2-20 7:54', 44, '6:43', 1
EXEC dbo.spAddStock 27, 'Cefotetan', '2020-6-9 14:12', 21, '7:24', 3
EXEC dbo.spAddStock 21, 'Chloramphenicol', '2025-4-24 6:14', 25, '9:20', 4
EXEC dbo.spAddStock 66, 'Clobetasol Propionate', '2024-1-22 11:4', 6, '2:3', 4
EXEC dbo.spAddStock 94, 'Fosamax', '2021-11-2 10:16', 84, '2:39', 4
EXEC dbo.spAddStock 50, 'Cancidas', '2021-7-22 14:6', 87, '17:41', 2
EXEC dbo.spAddStock 92, 'Clomid', '2022-8-16 8:26', 10, '20:12', 3
EXEC dbo.spAddStock 25, 'Cinoxacin', '2024-8-4 14:5', 65, '19:53', 1
EXEC dbo.spAddStock 9, 'Acetic Acid', '2023-1-20 6:26', 7, '13:55', 5
EXEC dbo.spAddStock 72, 'Butenafine', '2023-8-5 9:47', 52, '9:56', 1
EXEC dbo.spAddStock 10, 'Foscavir', '2021-1-25 12:47', 71, '17:23', 2
EXEC dbo.spAddStock 80, 'Clobetasol Propionate', '2021-3-7 9:10', 70, '16:15', 2
EXEC dbo.spAddStock 20, 'Edrophonium', '2024-11-20 7:56', 4, '22:56', 5
EXEC dbo.spAddStock 82, 'Doxercalciferol', '2025-4-17 7:36', 19, '12:21', 3
EXEC dbo.spAddStock 94, 'Fenofibric Acid', '2024-5-22 9:46', 88, '7:45', 1
EXEC dbo.spAddStock 21, 'Dicloxacillin', '2022-4-19 7:37', 1, '4:30', 1
EXEC dbo.spAddStock 61, 'Crofab', '2025-11-19 14:20', 56, '10:51', 1
EXEC dbo.spAddStock 71, 'Avinza', '2022-2-26 10:37', 0, '11:15', 5
EXEC dbo.spAddStock 63, 'Antizol', '2024-1-15 10:3', 100, '18:39', 4
EXEC dbo.spAddStock 10, 'Creon', '2020-5-27 11:47', 72, '3:55', 1
EXEC dbo.spAddStock 53, 'Aldesleukin', '2023-1-28 11:15', 64, '18:43', 1
EXEC dbo.spAddStock 36, 'Bismuth Subcitrate Potassium', '2024-10-1 6:8', 25, '6:41', 4
EXEC dbo.spAddStock 67, 'Acyclovir', '2024-7-23 6:22', 56, '6:6', 1
EXEC dbo.spAddStock 85, 'Folotyn', '2021-2-19 8:15', 96, '15:39', 5
EXEC dbo.spAddStock 63, 'Adoxa', '2020-5-21 8:0', 12, '12:38', 4
EXEC dbo.spAddStock 34, 'Aldesleukin', '2023-7-3 11:18', 61, '2:47', 2
EXEC dbo.spAddStock 21, 'Capastat Sulfate', '2022-11-24 9:23', 25, '11:41', 4
EXEC dbo.spAddStock 40, 'Combigan', '2024-12-4 8:18', 33, '11:23', 2
EXEC dbo.spAddStock 57, 'Amiloride and Hydrochlorothiazide', '2022-8-17 9:13', 56, '23:33', 5
EXEC dbo.spAddStock 75, 'Climara', '2025-2-17 9:18', 26, '10:48', 4
EXEC dbo.spAddStock 78, 'Fluoxymesterone', '2020-12-22 13:26', 73, '10:9', 4
EXEC dbo.spAddStock 33, 'Dexmedetomidine hydrochloride', '2023-9-13 9:38', 12, '5:50', 5
EXEC dbo.spAddStock 29, 'Cuvposa', '2025-3-14 8:18', 81, '13:34', 2
EXEC dbo.spAddStock 37, 'Celestone Soluspan', '2023-12-22 9:55', 39, '11:15', 3
EXEC dbo.spAddStock 70, 'Bumetanide', '2025-7-24 9:44', 22, '22:1', 4
EXEC dbo.spAddStock 95, 'Flecainide', '2023-12-17 9:10', 72, '17:15', 2
EXEC dbo.spAddStock 15, 'Dipentum', '2021-6-12 9:23', 57, '7:43', 4
EXEC dbo.spAddStock 60, 'Benzphetamine', '2022-5-14 13:53', 9, '7:35', 3
EXEC dbo.spAddStock 64, 'Cytotec', '2025-4-12 12:14', 29, '18:8', 1
EXEC dbo.spAddStock 32, 'Exemestane', '2025-5-17 14:36', 33, '2:38', 1
EXEC dbo.spAddStock 63, 'AndroGel', '2025-12-3 11:15', 10, '23:28', 3
EXEC dbo.spAddStock 86, 'Botulinum Toxin Type A', '2020-3-21 12:26', 62, '4:48', 3
EXEC dbo.spAddStock 40, 'Ciprodex', '2020-1-24 12:23', 19, '2:10', 5
EXEC dbo.spAddStock 56, 'EryPed', '2024-1-29 11:24', 49, '3:47', 5
EXEC dbo.spAddStock 85, 'Aci-Jel', '2024-7-21 11:58', 62, '16:55', 5
EXEC dbo.spAddStock 45, 'Cetirizine', '2021-5-14 10:26', 86, '21:52', 5
EXEC dbo.spAddStock 15, 'Alesse', '2021-3-8 9:4', 7, '7:51', 2
EXEC dbo.spAddStock 8, 'Fenofibric Acid', '2021-11-16 13:40', 4, '9:47', 5
EXEC dbo.spAddStock 56, 'Eptifibatide', '2022-1-12 13:5', 43, '23:6', 2
EXEC dbo.spAddStock 98, 'Flomax', '2021-1-27 11:7', 25, '2:24', 2
EXEC dbo.spAddStock 73, 'Eloxatin', '2023-4-8 8:15', 53, '1:53', 3
EXEC dbo.spAddStock 15, 'Clotrimazole', '2022-2-3 13:59', 43, '12:54', 1
EXEC dbo.spAddStock 21, 'Acular LS', '2021-11-25 7:44', 88, '13:53', 4
EXEC dbo.spAddStock 85, 'Bextra', '2020-5-29 7:7', 65, '12:37', 2
EXEC dbo.spAddStock 89, 'Alimta', '2023-12-5 10:48', 51, '9:10', 4
EXEC dbo.spAddStock 11, 'Duricef', '2021-6-14 11:4', 65, '23:19', 4
EXEC dbo.spAddStock 16, 'Econazole Nitrate', '2023-3-6 9:5', 5, '13:36', 1
EXEC dbo.spAddStock 48, 'Ancobon', '2025-7-28 9:42', 86, '15:22', 2
EXEC dbo.spAddStock 82, 'DMSO', '2024-3-25 13:23', 10, '7:23', 5
EXEC dbo.spAddStock 65, 'Ascorbic Acid', '2021-1-26 7:3', 13, '22:57', 1
EXEC dbo.spAddStock 19, 'Cerezyme', '2024-8-7 8:18', 90, '13:9', 3
EXEC dbo.spAddStock 4, 'Blenoxane', '2025-11-6 13:3', 48, '19:42', 5
EXEC dbo.spAddStock 8, 'Amitiza', '2022-5-28 10:59', 70, '12:31', 1
EXEC dbo.spAddStock 80, 'Frovatriptan Succinate', '2023-2-26 7:1', 14, '4:32', 2
EXEC dbo.spAddStock 41, 'Estramustine', '2025-3-12 8:25', 41, '17:49', 1
EXEC dbo.spAddStock 77, 'Eurax', '2022-4-27 9:7', 9, '20:44', 5
EXEC dbo.spAddStock 35, 'Bortezomib', '2022-7-30 11:16', 84, '23:44', 2
EXEC dbo.spAddStock 98, 'Cholestyramine', '2024-2-19 7:3', 91, '19:11', 4
EXEC dbo.spAddStock 2, 'Enoxacin', '2025-8-22 7:0', 62, '18:39', 3
EXEC dbo.spAddStock 12, 'Dihydroergotamine', '2020-4-17 14:24', 94, '1:18', 1
EXEC dbo.spAddStock 83, 'Actiq', '2020-12-3 14:25', 43, '12:52', 5
EXEC dbo.spAddStock 85, 'Ery-Tab', '2024-4-23 14:13', 93, '16:56', 5
EXEC dbo.spAddStock 45, 'Fluvastatin Sodium', '2024-4-14 14:37', 54, '2:42', 2
EXEC dbo.spAddStock 9, 'Elitek', '2025-9-14 11:58', 22, '3:45', 5
EXEC dbo.spAddStock 24, 'Fentanyl Buccal', '2024-3-10 13:20', 65, '4:3', 1
EXEC dbo.spAddStock 80, 'Aggrastat', '2020-3-30 11:39', 61, '20:58', 1
EXEC dbo.spAddStock 1, 'Cymbalta', '2023-2-11 9:20', 37, '0:36', 1
EXEC dbo.spAddStock 71, 'Cardura XL', '2021-8-28 14:2', 76, '18:40', 5
EXEC dbo.spAddStock 9, 'Fosinopril Sodium', '2023-12-21 14:53', 26, '21:46', 4
EXEC dbo.spAddStock 77, 'Eldepryl', '2024-6-1 13:46', 55, '5:55', 2
EXEC dbo.spAddStock 50, 'Colestid', '2022-7-29 7:7', 37, '15:41', 1
EXEC dbo.spAddStock 77, 'Acitretin', '2021-5-11 10:30', 50, '11:0', 2
EXEC dbo.spAddStock 47, 'Depakote', '2025-6-2 12:31', 31, '2:31', 3
EXEC dbo.spAddStock 91, 'Biltricide', '2024-4-14 8:19', 11, '13:44', 3
EXEC dbo.spAddStock 63, 'Aldactone', '2024-11-15 9:20', 9, '19:36', 1
EXEC dbo.spAddStock 54, 'Crofab', '2021-2-14 7:24', 15, '13:30', 1
EXEC dbo.spAddStock 14, 'Amaryl', '2025-2-28 13:33', 46, '6:58', 4
EXEC dbo.spAddStock 15, 'Detrol', '2022-1-15 14:36', 68, '3:45', 1
EXEC dbo.spAddStock 5, 'Climara', '2021-5-3 7:55', 10, '5:27', 5
EXEC dbo.spAddStock 24, 'Arcalyst', '2024-1-14 10:49', 36, '10:2', 2
EXEC dbo.spAddStock 6, 'Darvocet-N', '2022-7-9 13:33', 38, '4:53', 2
EXEC dbo.spAddStock 89, 'Bretylium', '2021-7-6 6:28', 27, '12:39', 1
EXEC dbo.spAddStock 54, 'Aldurazyme', '2022-3-24 9:58', 32, '3:1', 1
EXEC dbo.spAddStock 84, 'Diltiazem', '2021-2-2 10:40', 93, '21:13', 3
EXEC dbo.spAddStock 89, 'Bicillin C-R 900/300', '2023-10-26 9:4', 41, '19:50', 1
EXEC dbo.spAddStock 5, 'Elavil', '2022-8-7 6:17', 54, '21:8', 3
EXEC dbo.spAddStock 13, 'Azopt', '2021-12-1 6:57', 88, '7:53', 2
EXEC dbo.spAddStock 9, 'Exelon', '2024-6-8 13:3', 100, '4:32', 4
EXEC dbo.spAddStock 3, 'Extina', '2021-11-29 11:21', 8, '16:48', 2
EXEC dbo.spAddStock 26, 'Eulexin', '2023-2-7 7:6', 40, '10:48', 1
EXEC dbo.spAddStock 74, 'Diltiazem Hcl', '2024-1-10 13:18', 58, '7:24', 4
EXEC dbo.spAddStock 13, 'Cellulose', '2020-12-2 11:39', 27, '19:40', 2
EXEC dbo.spAddStock 57, 'Ery-Tab', '2024-10-19 12:1', 39, '17:56', 1
EXEC dbo.spAddStock 31, 'Aldurazyme', '2020-2-14 13:32', 10, '19:8', 4
EXEC dbo.spAddStock 70, 'Allopurinol', '2021-10-28 12:31', 96, '20:56', 4
EXEC dbo.spAddStock 64, 'Alprazolam', '2022-5-9 14:48', 61, '22:27', 5
EXEC dbo.spAddStock 10, 'Felodipine', '2022-7-25 13:24', 21, '6:31', 5
EXEC dbo.spAddStock 31, 'Compro', '2022-7-6 13:43', 18, '11:11', 5
EXEC dbo.spAddStock 59, 'Dilantin', '2022-1-27 10:45', 35, '2:54', 3
EXEC dbo.spAddStock 29, 'Avelox', '2025-7-12 8:24', 60, '2:56', 2
EXEC dbo.spAddStock 72, 'Emend', '2023-8-10 10:49', 83, '20:59', 1
EXEC dbo.spAddStock 97, 'Baycol', '2024-4-4 10:59', 69, '14:35', 5
EXEC dbo.spAddStock 80, 'Ceftin', '2023-1-7 13:57', 13, '18:26', 4
EXEC dbo.spAddStock 52, 'Elitek', '2023-1-21 10:50', 75, '3:22', 3
EXEC dbo.spAddStock 87, 'Flomax', '2021-7-13 6:9', 10, '4:43', 1
EXEC dbo.spAddStock 37, 'Cortaid', '2024-5-30 6:10', 37, '15:41', 2
EXEC dbo.spAddStock 12, 'Avinza', '2020-2-6 6:58', 89, '14:35', 3
EXEC dbo.spAddStock 9, 'Acticin', '2021-6-6 11:1', 1, '15:56', 1
EXEC dbo.spAddStock 44, 'Bivalirudin', '2024-6-13 10:31', 9, '15:5', 1
EXEC dbo.spAddStock 28, 'Acyclovir', '2023-10-24 6:22', 89, '20:30', 5
EXEC dbo.spAddStock 75, 'Cesamet', '2024-6-5 8:24', 78, '7:8', 3
EXEC dbo.spAddStock 52, 'Cortisone Acetate', '2024-10-19 13:11', 64, '3:47', 5
EXEC dbo.spAddStock 18, 'Aggrastat', '2022-11-6 8:17', 100, '9:16', 4
EXEC dbo.spAddStock 56, 'Chlorthalidone', '2024-9-12 9:35', 4, '22:3', 3
EXEC dbo.spAddStock 52, 'Delestrogen', '2025-12-27 12:30', 82, '1:55', 4
EXEC dbo.spAddStock 13, 'Alprazolam', '2022-6-10 7:26', 71, '15:51', 2
EXEC dbo.spAddStock 54, 'Ethosuximide', '2025-3-22 11:59', 97, '13:38', 1
EXEC dbo.spAddStock 94, 'Cellulose', '2024-6-6 9:23', 47, '1:56', 1
EXEC dbo.spAddStock 0, 'Ethrane', '2025-6-9 10:38', 69, '0:59', 1
EXEC dbo.spAddStock 6, 'Diltiazem Hcl', '2024-11-30 14:9', 53, '12:54', 5
EXEC dbo.spAddStock 84, 'Enjuvia', '2024-10-2 7:59', 14, '6:30', 4
EXEC dbo.spAddStock 6, 'Ethinyl Estradiol', '2025-10-5 7:59', 39, '2:3', 5
EXEC dbo.spAddStock 99, 'Fosamax', '2023-4-14 7:28', 29, '19:55', 3
EXEC dbo.spAddStock 54, 'Curosurf', '2024-6-29 7:20', 80, '6:25', 5
EXEC dbo.spAddStock 4, 'Dipentum', '2025-7-28 9:5', 76, '9:13', 2
EXEC dbo.spAddStock 34, 'Axid', '2025-1-20 14:40', 34, '0:38', 5
EXEC dbo.spAddStock 96, 'Cervidil', '2021-7-27 12:41', 87, '11:40', 5
EXEC dbo.spAddStock 78, 'Captopril', '2023-8-22 13:45', 80, '2:42', 5
EXEC dbo.spAddStock 64, 'Bacitracin', '2021-7-29 8:6', 11, '1:56', 3
EXEC dbo.spAddStock 88, 'Alefacept', '2020-6-25 10:54', 67, '12:13', 2
EXEC dbo.spAddStock 42, 'Desogestrel and Ethinyl Estradiol', '2020-5-22 13:11', 70, '2:51', 3
EXEC dbo.spAddStock 11, 'Alimta', '2020-6-10 8:40', 87, '4:59', 5
EXEC dbo.spAddStock 48, 'Cialis', '2022-5-12 13:38', 41, '13:41', 3
