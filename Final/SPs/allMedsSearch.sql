alter FUNCTION dbo.allMedsSearch ( @pMedName VARCHAR(32) ) RETURNS TABLE
AS
RETURN
	SELECT *
	FROM MEDICINE
	WHERE MEDICINE_UNIQUE_NAME LIKE '%'+@pMedName+'%'

--use MyMeds
-- select * from dbo.allMedsSearch('A')