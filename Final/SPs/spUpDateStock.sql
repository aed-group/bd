CREATE PROCEDURE dbo.spUpDateStock
	@userid		    INT,
	@medicineUname  VARCHAR(64),
	@oldDate		VARCHAR(16),
	@qnty			INT
AS
BEGIN
	UPDATE STOCK
	SET QUANTITY = @qnty
	WHERE USER_ID = @userid AND MEDICINE_U_NAME = @medicineUname AND EXPIRATION_DATE = @oldDate
END

-- EXEC dbo.spUpDateStock 10, 'Acetic Acid', '2023-30-6'




