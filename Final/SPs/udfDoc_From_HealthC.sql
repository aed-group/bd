--para 1 health center lista de todos os Medicos 

--use MyMeds
create Function dbo.udfDoc_From_HealthC (@hcname As VARCHAR(32)) RETURNS TABLE
As
return
	Select DOCTOR.DOCTOR_ID, DOCTOR.DOCTOR_NAME, DOCTOR.EXPERTISE_AREA, DOCTOR_EMAIL, DOCTOR_PHONE
	from DOCTOR, HEALTH_CENTER, WORKS_ON
	where HEALTH_CENTER.HEALTH_CENTER_NAME = @hcname and HEALTH_CENTER.HEALTH_CENTER_NAME = WORKS_ON.HEALTH_CENTER_NAME and WORKS_ON.DOCTOR_ID = DOCTOR.DOCTOR_ID

--select * from dbo.udfDoc_From_HealthC ('Cuf')