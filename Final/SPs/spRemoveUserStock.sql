CREATE PROCEDURE dbo.spRemoveUserStock
    @pUserId INT, 
    @pMedName VARCHAR(64),
    @responseMessage NVARCHAR(250) = NULL OUTPUT
AS
BEGIN
    SET NOCOUNT ON

    BEGIN TRY

        DELETE FROM STOCK
		WHERE USER_ID = @pUserId and MEDICINE_U_NAME = @pMedName

        SET @responseMessage='Success'

    END TRY
    BEGIN CATCH
        SET @responseMessage=ERROR_MESSAGE() 
    END CATCH

END