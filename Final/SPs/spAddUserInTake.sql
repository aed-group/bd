CREATE PROCEDURE dbo.spAddUserInTake
    @pUserId INT, 
    @pMedicineName VARCHAR(64), 
    @pTakeHour VARCHAR(16), 
	@pTakeQantity INT,
    @responseMessage NVARCHAR(250) = NULL OUTPUT
AS
BEGIN
    SET NOCOUNT ON

    BEGIN TRY

        INSERT INTO TAKE_MEDICINE
        VALUES(@pUserId, @pMedicineName, @pTakeHour, @pTakeQantity)

        SET @responseMessage='Success'

    END TRY
    BEGIN CATCH
        SET @responseMessage=ERROR_MESSAGE() 
    END CATCH

END