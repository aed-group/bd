CREATE PROCEDURE dbo.spRemoveUser
	@uId	   INT
AS
BEGIN
	DELETE FROM USER_ACC
	WHERE USER_ID = @uId
END

--use mymeds
-- EXEC dbo.spRemoveUser 8