--para 1 medico lista de health centers onde trabalha

--use MyMeds
create Function dbo.udfHealth_From_Doc (@DocID As Int) RETURNS TABLE
As
return
	Select HEALTH_CENTER.HEALTH_CENTER_NAME
	from DOCTOR, HEALTH_CENTER, WORKS_ON
	where DOCTOR.DOCTOR_ID = @DocID and WORKS_ON.DOCTOR_ID = DOCTOR.DOCTOR_ID and HEALTH_CENTER.HEALTH_CENTER_NAME = WORKS_ON.HEALTH_CENTER_NAME 

--select * from dbo.udfHealth_From_Doc (0)