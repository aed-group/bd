CREATE FUNCTION dbo.getSupervisee ( @pUserId INT ) RETURNS TABLE
AS
RETURN
	SELECT *
	FROM USER_ACC
	WHERE SUPERVISOR_ID = @pUserId

-- select * from dbo.getSupervisee(0)