CREATE PROCEDURE dbo.upDateInTake
	@userid		   INT,
	@medicineUname VARCHAR(64),
	@oldhour		   VARCHAR(16),
	@newhour		   VARCHAR(16),
	@qnty			INT
AS
BEGIN
	UPDATE TAKE_MEDICINE
	SET TAKE_HOUR = @newhour, TAKE_QUANTITY = @qnty
	WHERE USER_ID = @userid AND MEDICINE_U_NAME = @medicineUname AND TAKE_HOUR = @oldhour
END
