CREATE PROCEDURE dbo.spAddDoctor
    @pId INT, 
    @pPassword NVARCHAR(50), 
    @pDoctorName VARCHAR(32), 
	@pDoctorArea VARCHAR(16),
    @pDoctorEmail VARCHAR(32),
	@pDoctorPhone VARCHAR(14),
    @responseMessage NVARCHAR(250) = NULL OUTPUT
AS
BEGIN
    SET NOCOUNT ON

    BEGIN TRY

        INSERT INTO DOCTOR
        VALUES(@pId, HASHBYTES('SHA2_512', @pPassword), @pDoctorName, @pDoctorArea, @pDoctorEmail, @pDoctorPhone)

        SET @responseMessage='Success'

    END TRY
    BEGIN CATCH
        SET @responseMessage=ERROR_MESSAGE() 
    END CATCH

END