CREATE FUNCTION dbo.Login ( @pLoginEmail VARCHAR(32), @pLoginPassword NVARCHAR(50) ) RETURNS VARCHAR(32)
AS
BEGIN

    DECLARE @returnMessage VARCHAR(10) = 'I'
	DECLARE @userId INT

    IF EXISTS (SELECT TOP 1 USER_ID FROM USER_ACC WHERE USER_EMAIL=@pLoginEmail)
    BEGIN
       SET @userId=(SELECT USER_ID FROM USER_ACC WHERE USER_EMAIL=@pLoginEmail AND PASSWORD_HASH=HASHBYTES('SHA2_512', @pLoginPassword))

       IF(@userId IS NULL)
		   SET @returnMessage='I/P'
	   ELSE
           SET @returnMessage=CONCAT('U ', CONVERT(VARCHAR(3), @userId))
    END
    
	IF EXISTS (SELECT TOP 1 DOCTOR_ID FROM DOCTOR WHERE DOCTOR_EMAIL=@pLoginEmail)
    BEGIN
       SET @userId=(SELECT DOCTOR_ID FROM DOCTOR WHERE DOCTOR_EMAIL=@pLoginEmail AND PASSWORD_HASH=HASHBYTES('SHA2_512', @pLoginPassword))

       IF(@userId IS NULL)
		   SET @returnMessage='I/P'
	   ELSE
           SET @returnMessage=CONCAT('D ', CONVERT(VARCHAR(3), @userId))
    END

	RETURN @returnMessage
END