CREATE FUNCTION dbo.getFavouriteHCByUSer ( @pLoginId INT ) RETURNS TABLE
AS
RETURN
	SELECT TOP 1 *
	FROM dbo.getUserAppointmentsHCCount(@pLoginId)
	WHERE HC_COUNT = (	SELECT MAX(HC_COUNT) AS MAX_HC
						FROM				(	SELECT * 
												FROM dbo.getUserAppointmentsHCCount(@pLoginId)	) AS TEMP )