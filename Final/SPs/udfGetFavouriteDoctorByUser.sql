CREATE FUNCTION dbo.getFavouriteDoctorByUser ( @pLoginId INT ) RETURNS TABLE
AS
RETURN
	SELECT TOP 1 *
	FROM dbo.getUserAppointmentsDoctorCount(@pLoginId)
	WHERE APP_COUNT = (	SELECT MAX(APP_COUNT) AS MAX_DOCTOR
						FROM				(	SELECT * 
												FROM dbo.getUserAppointmentsDoctorCount(@pLoginId)	) AS TEMP )

--select * from dbo.getFavouriteDoctorByUser(10)
