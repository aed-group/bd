CREATE PROCEDURE dbo.spAddUser
    @pId INT, 
    @pPassword NVARCHAR(50), 
	@pSupervisorId INT,
    @pUserName VARCHAR(32), 
    @pUserEmail VARCHAR(32),
    @responseMessage NVARCHAR(250) = NULL OUTPUT
AS
BEGIN
    SET NOCOUNT ON

    BEGIN TRY

        INSERT INTO USER_ACC (USER_ID, PASSWORD_HASH, SUPERVISOR_ID, USER_NAME, USER_EMAIL)
        VALUES(@pId, HASHBYTES('SHA2_512', @pPassword), @pSupervisorId, @pUserName, @pUserEmail)

        SET @responseMessage='Success'

    END TRY
    BEGIN CATCH
        SET @responseMessage=ERROR_MESSAGE() 
    END CATCH

END

--EXEC dbo.spAddUser 0, '12345', 0, 'Jo�o Marques', 'joao.marques@gmail.com'