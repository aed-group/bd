CREATE PROCEDURE dbo.spRemoveUserTakes
	@userid		   INT,
	@medicineUname VARCHAR(64),
	@hour		   VARCHAR(16)
AS
BEGIN
	DELETE FROM TAKE_MEDICINE
	WHERE USER_ID = @userid AND MEDICINE_U_NAME = @medicineUname AND TAKE_HOUR = @hour
END

--drop procedure dbo.deleteInTake