CREATE PROCEDURE dbo.spAddStock
    @pUserId INT, 
    @pMedicineUName VARCHAR(64), 
    @pExpirationDate DATE, 
	@pQuantity INT,
    @pTakeHour VARCHAR(16),
	@pTakeQuantity INT,
    @responseMessage NVARCHAR(250) = NULL OUTPUT
AS
BEGIN
    SET NOCOUNT ON

    BEGIN TRY

        INSERT INTO STOCK
        VALUES(@pUserId, @pMedicineUName, @pExpirationDate, @pQuantity)

		-- TAKE_MEDICINE tuple is created with trigger

		UPDATE TAKE_MEDICINE
		SET TAKE_HOUR = @pTakeHour, TAKE_QUANTITY = @pTakeQuantity
		WHERE USER_ID = @pUserId AND MEDICINE_U_NAME = @pMedicineUName 

        SET @responseMessage='Success'

    END TRY
    BEGIN CATCH
        SET @responseMessage=ERROR_MESSAGE() 
    END CATCH

END