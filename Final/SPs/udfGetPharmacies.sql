CREATE FUNCTION dbo.getPharmacies ( @pCity VARCHAR(32) ) RETURNS TABLE
AS
RETURN
	SELECT *
	FROM PHARMACY
	WHERE PHARMACY_ADDRESS LIKE '%'+@pCity+'%'

--use MyMeds select * from dbo.getPharmacies('Porto')
