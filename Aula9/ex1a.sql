
--create procedure get_dept_man

alter procedure get_dept_man
@man_ssn int output, 
@man_years int output
as	
	declare @min_date date
	
	select @min_date = MIN(Mgr_start_date)  
	from Company.department 
	where exists (
		select Mgr_ssn, Mgr_start_date 
		from Company.employee, Company.department
		where Mgr_ssn = Ssn
	) and Mgr_ssn is not null

	print @min_date

	declare @man_ssn int

	select @man_ssn = Mgr_ssn
	from Company.department
	where Mgr_start_date = @min_date

	print @man_ssn

	--Datediff


 -- exec get_dept_man