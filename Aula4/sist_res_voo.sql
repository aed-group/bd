CREATE SCHEEMA FLIGHT_MAN;

CREATE TABLE FLIGHT_MAN.AIRPORT (
  AirportCode         INT             PRIVATE KEY,
  City                VARCHAR(10),
  State               VARCHAR(10),
  Name                VARCHAR(15)
);

CREATE TABLE FLIGHT_MAN.FLIGHT (
  FNumber               INT             PRIMARY KEY,
  Airline               VARCHAR(10),
  Weekdays              INT
);

CREATE TABLE FLIGHT_MAN.FLIGHT_LEG (
  Leg_no                    INT             PRIMARY KEY,
  FlightNumber              INT             REFERENCES FLIGHT_MAN.FLIGHT(FNumber),
  Departure_Airport_Code    INT             REFERENCES FLIGHT_MAN.AIRPORT(AirportCode),
  Arrival_Airport_Code      INT             REFERENCES FLIGHT_MAN.AIRPORT(AirportCode),
  Scheduled_Dept_Time       INT,
  Scheduled_Arr_Time        INT
);

CREATE TABLE FLIGHT_MAN.FARE (
  Code                      INT             PRIMARY KEY,
  Flight_Number             INT             REFERENCES FLIGHT_MAN.FLIGHT(FNumber),
  Restrictions              VARCHAR(20),
  Amount                    INT
);

CREATE TABLE FLIGHT_MAN.LEG_INSTANCE (
  LegDate                   VARCHAR(10)     PRIMARY KEY,
  Flight_Number             INT             REFERENCES FLIGHT_MAN.FLIGHT(FNumber),
  Airplane_ID               INT             REFERENCES FLIGHT_MAN.AIRPLANE(Airplane_ID),
  No_Available_Seats        BOOLEAN,
  Dept_Airport_Code         INT             REFERENCES FLIGHT_MAN.AIRPORT(AirportCode),
  Arr_Airport_Code          INT             REFERENCES FLIGHT_MAN.AIRPORT(AirportCode),
  Arr_time                  INT,
  Dept_time                 INT
);

CREATE TABLE FLIGHT_MAN.SEAT (
  Seat_no                   INT             PRIMARY KEY,
  Flight_Number             INT             REFERENCES FLIGHT_MAN.FLIGHT(FNumber),
  Cphone                    VARCHAR(10),
  Customer_name             VARCHAR(20)
);

CREATE TABLE FLIGHT_MAN.AIRPLANE_TYPE (
  Type_name                 VARCHAR(10)     PRIMARY KEY,
  Company                   VARCHAR,
  Max_seats                 INT
);

CREATE TABLE FLIGHT_MAN.AIRPLANE (
  Airplane_ID               INT             PRIMARY KEY,
  Total_no_seats            INT,
  Airplane_type_name        VARCHAR(10)     REFERENCES FLIGHT_MAN.AIRPLANE_TYPE(Type_name)
);

CREATE TABLE FLIGHT_MAN.CAN_LAND (
  AirportCode               INT              REFERENCES FLIGHT_MAN.AIRPORT(AirportCode),
  Airplane_type_name        VARCHAR(10)      REFERENCES FLIGHT_MAN.AIRPLANE(Type_name)
);
