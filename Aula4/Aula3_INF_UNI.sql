--CREATE SCHEMA INF_UNI;

CREATE TABLE INF_UNI.PROFESSOR (
	Nmec				INT					PRIMARY KEY,
	Data_Nasc			DATE,
	Categoria_Prof		CHAR(15),
	Nome				CHAR(15),
	Area_Cient			CHAR(15),
	Dep_nome			CHAR(15),			--REFERENCES INF_UNI.DEPARTAMENTO(Nome),
	Dedicacao			INT
);

CREATE TABLE INF_UNI.DEPARTAMENTO (
	Nome				CHAR(15)			PRIMARY KEY,
	Localizacao			CHAR(15)			NOT NULL,
	Nmec_Prof			INT					REFERENCES INF_UNI.PROFESSOR(Nmec)
);

CREATE TABLE INF_UNI.PROJECTO (
	Id					INT					PRIMARY KEY,
	Entidade_finaceira	CHAR(15),
	Nome				CHAR(15),
	Data_inicio			DATE,
	Data_termino		DATE,
	Orcamento			INT					NOT NULL,
	Nmec_Prof_Gestor	INT					REFERENCES INF_UNI.PROFESSOR(Nmec)
);

CREATE TABLE INF_UNI.ESTUDANTE (
	Nmec				INT					PRIMARY KEY,
	Nome				CHAR(15),
	Data_Nasc			DATE,
	Grau_Formacao		CHAR(15),
	Dep_nome			CHAR(15)			REFERENCES INF_UNI.DEPARTAMENTO(Nome),
	Nmec_Advisor		INT					REFERENCES INF_UNI.ESTUDANTE(Nmec)
);

CREATE TABLE INF_UNI.PARTICIPACAO_ESTUDANTE (
	Id_Projecto			INT,
	Nmec_Estudante		INT,
	PRIMARY KEY(Id_Projecto,Nmec_Estudante),
	FOREIGN KEY(Id_Projecto) REFERENCES INF_UNI.PROJECTO(Id),
	FOREIGN KEY(Nmec_Estudante) REFERENCES INF_UNI.ESTUDANTE(Nmec)
);

CREATE TABLE INF_UNI.SUPERVISIONAMENTO (
	Id_Projecto			INT,
	Nmec_Estudante		INT,
	Nmec_Professor		INT,
	PRIMARY KEY (Id_Projecto, Nmec_Estudante, Nmec_Professor),
	FOREIGN KEY (Id_Projecto)				REFERENCES INF_UNI.PARTICIPACAO_ESTUDANTE(Id_Projecto), --Syntax??
	FOREIGN KEY(Nmec_Estudante)				REFERENCES INF_UNI.PARTICIPACAO_ESTUDANTE(Nmec_Estudante),
	FOREIGN KEY(Nmec_Professor)				REFERENCES INF_UNI.PROFESSOR(Nmec)
);	

CREATE TABLE INF_UNI.PARTICIPACAO_PROFESSOR (
	Id_Projecto			INT,
	Nmec_Professor		INT,
	PRIMARY KEY (Id_Projecto, Nmec_Professor),
	FOREIGN KEY(Id_Projecto)				REFERENCES INF_UNI.PROJECTO(Id),
	FOREIGN KEY(Nmec_Professor)				REFERENCES INF_UNI.PROFESSOR(Nmec)
);





