-- CREATE SCHEMA Company;


CREATE TABLE Company.department ( Dname text,
                          Dnumber int PRIMARY KEY NOT NULL,
                          Mgr_ssn int,
                          Mgr_start_date date
                          );

CREATE TABLE Company.employee (	Fname text,
                        Minit text,
                        Lname text,
                        Ssn int PRIMARY KEY NOT NULL,
                        Bdate date,
                        Address text,
                        Sex text,
                        Salary int,
                        Super_ssn int REFERENCES Company.employee(Ssn),
                        Dno int	REFERENCES Company.department(Dnumber)
                        );

CREATE TABLE Company.dependent ( Essn int REFERENCES Company.employee(Ssn),
                         Dependent_name text,
                         Sex text,
                         Bdate date,
                         Relationship text,
                         );

CREATE TABLE Company.dept_location ( Dnumber int PRIMARY KEY REFERENCES Company.department(Dnumber),
                             Dlocation text,
                             );

CREATE TABLE Company.project ( Pname text,
                       Pnumber int PRIMARY KEY NOT NULL,
                       Plocation text,
                       Dnum int	REFERENCES Company.department(Dnumber),
		       );

CREATE TABLE Company.works_on ( Essn int REFERENCES Company.employee(Ssn),
                        Pno int	REFERENCES Company.project(Pnumber),
                        Hours int,
                        );



